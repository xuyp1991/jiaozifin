/**
 *  @file
 *  @copyright defined in eos/LICENSE.txt
 */
#pragma once

#include <eosiolib/asset.hpp>
#include <eosiolib/eosio.hpp>
#include <eosiolib/crypto.h>
#include <string>

namespace eosiosystem {
   class system_contract;
}

namespace jiaozi {

   using std::string;
   using namespace eosio;
   class token : public contract {
      public:
         token( account_name self ):contract(self){}

         void create( account_name issuer,
                      asset        maximum_supply,
                      asset        value,
                      string       memo);

         void issue( account_name to, asset quantity, string memo );

         void transfer( account_name from,
                        account_name to,
                        asset        quantity,
                        string       memo );

         void fee( account_name payer, asset quantity );

         void setprice(asset base,asset quote,string memo);
         void destroy(const account_name owner,asset quantity,string memo);
         void setvalue(asset base,asset value,string memo);
         void calvalue(asset base,string memo);
         void calprice(asset base,string memo);
         
         void confirm(string filemd5,string memo);
         void cancel(string filemd5,string memo);
      
         inline asset get_supply( symbol_name sym )const;
         
         inline asset get_balance( account_name owner, symbol_name sym )const;

      private:
         static key256 get_key_id(const string &key_str) {
            checksum256 id_hash;
            sha256(key_str.c_str(),key_str.length(),&id_hash);
            const uint64_t *p64 = reinterpret_cast<const uint64_t *>(&id_hash);
            return key256::make_from_word_sequence<uint64_t>(p64[0], p64[1], p64[2], p64[3]); 
         }

         struct account {
            asset    balance;

            uint64_t primary_key()const { return balance.symbol.name(); }
         };

         struct currency_stats {
            asset          supply;
            asset          max_supply;
            account_name   issuer;

            asset          total_value;

            uint64_t primary_key()const { return supply.symbol.name(); }
         };

         struct stat_price {
            asset    price;

            uint64_t primary_key()const { return price.symbol.name(); }
         };

         struct exchange_item {
            account_name trader;
            asset   quantity;

            EOSLIB_SERIALIZE(exchange_item, ( trader )(quantity))
         };

         struct multiple_exchange {
            exchange_item base;
            vector<exchange_item> quote;
            uint64_t id;
            string filemd5;
            uint32_t record_property;
            uint32_t current_block;

            uint64_t primary_key()const { return id; }
            key256 get_index_byhash()const { 
               return get_key_id(filemd5);
            }
         };


         typedef eosio::multi_index<N(accounts), account> accounts;
         typedef eosio::multi_index<N(stat), currency_stats> stats;
         typedef eosio::multi_index<N(price), stat_price> currency_price;
         typedef eosio::multi_index<N(transrecord), multiple_exchange,
         indexed_by< N(byhash),
                  eosio::const_mem_fun<multiple_exchange, key256, &multiple_exchange::get_index_byhash >>> exchange_records;
         typedef eosio::multi_index<N(transdeal), multiple_exchange,
         indexed_by< N(byhash),
                  eosio::const_mem_fun<multiple_exchange, key256, &multiple_exchange::get_index_byhash >>> exchange_deals;

         void sub_balance( account_name owner, asset value );
         void add_balance( account_name owner, asset value, account_name ram_payer );

      public:

      void exchange(string filemd5,const exchange_item base,const vector<exchange_item> quote,uint32_t exchange_property,string memo);

   };

   asset token::get_supply( symbol_name sym )const
   {
      stats statstable( _self, sym );
      const auto& st = statstable.get( sym );
      return st.supply;
   }

   asset token::get_balance( account_name owner, symbol_name sym )const
   {
      accounts accountstable( _self, owner );
      const auto& ac = accountstable.get( sym );
      return ac.balance;
   }

} /// namespace eosio
