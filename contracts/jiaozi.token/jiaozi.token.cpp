/**
 *  @file
 *  @copyright defined in eos/LICENSE.txt
 */

#include "jiaozi.token.hpp"


namespace jiaozi {

   inline int64_t precision(uint64_t decimals)
   {
      int64_t p10 = 1;
      int64_t p = (int64_t)decimals;
      while( p > 0  ) {
         p10 *= 10; --p;
      }
      return p10;
   }

void token::create( account_name issuer,
                    asset        maximum_supply,
                    asset        value ,
                    string       memo)
{
    require_auth( _self );

    auto sym = maximum_supply.symbol;
    eosio_assert( sym.is_valid(), "invalid symbol name" );
    eosio_assert( maximum_supply.is_valid(), "invalid supply");
    eosio_assert( maximum_supply.amount > 0, "max-supply must be positive");
    eosio_assert( sym != CORE_SYMBOL, "not allow create core symbol token by token contract");

    stats statstable( _self, sym.name() );
    auto existing = statstable.find( sym.name() );
    eosio_assert( existing == statstable.end(), "token with symbol already exists" );

    statstable.emplace( _self, [&]( auto& s ) {
       s.supply.symbol = maximum_supply.symbol;
       s.max_supply    = maximum_supply;
       s.issuer        = issuer;
       s.total_value   = value;
    });
}


void token::issue( account_name to, asset quantity, string memo )
{
    auto sym = quantity.symbol;
    eosio_assert( sym.is_valid(), "invalid symbol name" );
    eosio_assert( memo.size() <= 256, "memo has more than 256 bytes" );

    auto sym_name = sym.name();
    stats statstable( _self, sym_name );
    auto existing = statstable.find( sym_name );
    eosio_assert( existing != statstable.end(), "token with symbol does not exist, create token before issue" );
    const auto& st = *existing;

    require_auth( _self );
    eosio_assert( quantity.is_valid(), "invalid quantity" );
    eosio_assert( quantity.amount > 0, "must issue positive quantity" );

    eosio_assert( quantity.symbol == st.supply.symbol, "symbol precision mismatch" );
    //eosio_assert( quantity.amount <= st.max_supply.amount - st.supply.amount, "quantity exceeds available supply");

    statstable.modify( st, 0, [&]( auto& s ) {
       s.supply += quantity;
    });

    add_balance( st.issuer, quantity, st.issuer );

    if( to != st.issuer ) {
       SEND_INLINE_ACTION( *this, transfer, {_self,N(active)}, {st.issuer, to, quantity, memo} );
    }
}

void token::transfer( account_name from,
                      account_name to,
                      asset        quantity,
                      string       memo )
{
    eosio_assert( from != to, "cannot transfer to self" );
    require_auth( _self );
    eosio_assert( is_account( to ), "to account does not exist");
    auto sym = quantity.symbol.name();
    stats statstable( _self, sym );
    const auto& st = statstable.get( sym );

    require_recipient( from );
    require_recipient( to );

    eosio_assert( quantity.is_valid(), "invalid quantity" );
    eosio_assert( quantity.amount > 0, "must transfer positive quantity" );
    eosio_assert( quantity.symbol == st.supply.symbol, "symbol precision mismatch" );
    eosio_assert( memo.size() <= 256, "memo has more than 256 bytes" );


    sub_balance( from, quantity );
    add_balance( to, quantity, from );
}

void token::fee( account_name payer, asset quantity ){
   // account to get fee, TODO By FanYang : need use conf
   const auto fee_account = N(force.fee);

   require_auth( _self );

   auto sym = quantity.symbol.name();
   stats statstable( _self, sym );
   const auto& st = statstable.get( sym );

   eosio_assert( quantity.is_valid(), "invalid quantity" );
   eosio_assert( quantity.amount > 0, "must transfer positive quantity" );
   eosio_assert( quantity.symbol == st.supply.symbol, "symbol precision mismatch" );

   sub_balance( payer, quantity );
   add_balance( fee_account, quantity, payer );
}

void token::sub_balance( account_name owner, asset value ) {
   accounts from_acnts( _self, owner );

   const auto& from = from_acnts.get( value.symbol.name(), "no balance object found" );
   eosio_assert( from.balance.amount >= value.amount, "overdrawn balance" );

   if( from.balance.amount == value.amount ) {
      from_acnts.erase( from );
   } else {
      from_acnts.modify( from, 0, [&]( auto& a ) {
          a.balance -= value;
      });
   }
}

void token::add_balance( account_name owner, asset value, account_name ram_payer )
{
   accounts to_acnts( _self, owner );
   
   auto to = to_acnts.find( value.symbol.name() );
   if( to == to_acnts.end() ) {
      to_acnts.emplace( _self, [&]( auto& a ){
        a.balance = value;
      });
   } else {
      to_acnts.modify( to, 0, [&]( auto& a ) {
        a.balance += value;
      });
   }
}

void token::setprice(asset base,asset quote,string memo) {

   auto sym_name = base.symbol.name();
   stats statstable( _self, sym_name );
   auto existing = statstable.find( sym_name );
   eosio_assert( existing != statstable.end(), "token with symbol does not exist, create token before setprice" );
   eosio_assert(existing->total_value.symbol == quote.symbol,"price must be the same as the value symbol");
   require_auth( _self );
   //auto sym_name = base.symbol.name();
   currency_price price_tbl( _self, sym_name );
   auto exist = price_tbl.find(quote.symbol.name());

   eosio_assert( memo.size() <= 256, "memo has more than 256 bytes" );
   if (exist == price_tbl.end()) {
      price_tbl.emplace( _self, [&]( auto& a ){
         a.price = quote;
      });
   }
   else {
      price_tbl.modify(exist, 0, [&]( auto& a ) {
         a.price = quote;
      });
   }
}

void token::destroy(const account_name owner,asset quantity,string memo) {
   require_auth( _self );
   accounts from_acnts( _self, owner );
   auto sym_name = quantity.symbol.name();
   auto from = from_acnts.find( sym_name );
   eosio_assert( from != from_acnts.end(), "the account do not have the token" );
   eosio_assert( from->balance >= quantity, "the account do not have enough token" );
   
   stats statstable( _self, sym_name );
   auto existing = statstable.find( sym_name );
   eosio_assert( existing != statstable.end(), "token with symbol does not exist" );

   sub_balance(owner,quantity);
   statstable.modify(existing,0,[&](auto &a) {
      a.supply -= quantity;
   });

}


void token::setvalue(asset base,asset value,string memo) {
   auto sym_name = base.symbol.name();
   stats statstable( _self, sym_name );
   auto existing = statstable.find( sym_name );
   eosio_assert( existing != statstable.end(), "token with symbol does not exist, create token before setprice" );
   require_auth( _self );

   //auto core_symbol = asset(0).symbol;
   eosio_assert(existing->total_value.symbol == value.symbol,"value must be the same as the value symbol");
   statstable.modify(existing,0,[&](auto &a) {
      a.total_value = value;
   });
}

void token::calvalue(asset base,string memo) {
   auto sym_name = base.symbol.name();
   stats statstable( _self, sym_name );
   auto existing = statstable.find( sym_name );
   eosio_assert( existing != statstable.end(), "token with symbol does not exist, create token before setprice" );
   require_auth( _self );

   currency_price price_tbl( _self, sym_name );
   auto exist = price_tbl.find(existing->total_value.symbol.name());
   eosio_assert( exist != price_tbl.end(), "can not find price" );

   auto value = existing->supply.amount * exist->price / precision(existing->supply.symbol.precision());
   statstable.modify(existing,0,[&](auto &a) {
      a.total_value = value;
   });
}

void token::calprice(asset base,string memo) {
   auto sym_name = base.symbol.name();
   stats statstable( _self, sym_name );
   auto existing = statstable.find( sym_name );
   eosio_assert( existing != statstable.end(), "token with symbol does not exist, create token before setprice" );
   require_auth( _self );

   currency_price price_tbl( _self, sym_name );
   auto exist = price_tbl.find(existing->total_value.symbol.name());

   auto price = existing->total_value * precision(existing->supply.symbol.precision()) / existing->supply.amount;
   if (exist == price_tbl.end()) {
      price_tbl.emplace( _self, [&]( auto& a ){
         a.price = price;
      });
   }
   else {
      price_tbl.modify(exist, 0, [&]( auto& a ) {
         a.price = price;
      });
   }
}

void token::exchange(string filemd5,const exchange_item base,const vector<exchange_item> quote,uint32_t exchange_property,string memo) {
   eosio_assert(filemd5 != string(""),"the file md5 must not be empty");
   require_auth( _self);
   exchange_records record_tbl(_self,_self);

   auto idx = record_tbl.get_index<N(byhash)>();
   auto to = idx.find(get_key_id(filemd5));
   eosio_assert(to == idx.end(),"the file md5 exchange has exist");

   exchange_deals deal_tbl(_self,_self);
   auto deal_idx = deal_tbl.get_index<N(byhash)>();
   auto deal_to = deal_idx.find(get_key_id(filemd5));
   eosio_assert(deal_to == deal_idx.end(),"the file md5 deal has exist");

   auto isize = quote.size();
   eosio_assert(isize > 0,"the quote size must be bigger then zero");
   auto quote_symbol = quote[0].quantity.symbol;
   for (auto  itr = quote.begin();itr != quote.end();++itr) {
      eosio_assert(itr->quantity.symbol == quote_symbol,"the quote coin is not the same coin");
   }

   record_tbl.emplace( _self, [&]( auto& s ) {
      s.id = record_tbl.available_primary_key();
      s.base = base;
      s.quote.assign(quote.begin(),quote.end());
      s.filemd5 = filemd5;
      s.record_property = exchange_property;
      s.current_block = current_block_num();
   });

   INLINE_ACTION_SENDER(jiaozi::token, transfer)( _self, {{_self,N(active)}},
                                    { base.trader,_self,base.quantity,memo} );

   for (auto itr = quote.begin();itr != quote.end(); ++itr) {
      INLINE_ACTION_SENDER(jiaozi::token, transfer)( _self, {{_self,N(active)}},
                                    { itr->trader,_self,itr->quantity,memo} );
   }

}

void token::confirm(string record_id,string memo) {
   eosio_assert(record_id != string(""),"the record id must not be empty");
   require_auth( _self);
   exchange_records record_tbl(_self,_self);

   auto idx = record_tbl.get_index<N(byhash)>();
   auto to = idx.find(get_key_id(record_id));
   eosio_assert(to != idx.end(),"the contract do not exist");

   //string memo = "confirm exchange"
   memo = "confirm exchange -- " + memo;


   auto total_quote = asset(0,to->quote[0].quantity.symbol);
   auto itr = to->quote.begin();
   for (itr = to->quote.begin();itr != to->quote.end();++itr) {
      total_quote += itr->quantity;
   }

   for (itr = to->quote.begin();itr != to->quote.end();++itr) {
      auto quantity_amount = static_cast<int128_t>(to->base.quantity.amount) * static_cast<int128_t>(itr->quantity.amount) / static_cast<int128_t>(total_quote.amount);
      auto trade_quantity = asset(static_cast<int64_t>(quantity_amount),to->base.quantity.symbol);
      INLINE_ACTION_SENDER(jiaozi::token, transfer)( _self, {{_self,N(active)}},
                                          { _self,itr->trader,trade_quantity ,memo} );
      INLINE_ACTION_SENDER(jiaozi::token, transfer)( _self, {{ _self,N(active)}},
                                          { _self,to->base.trader,itr->quantity,memo} );
   }

   exchange_deals deal_tbl(_self,_self);
   auto deal_idx = deal_tbl.get_index<N(byhash)>();
   auto deal_to = deal_idx.find(get_key_id(record_id));
   eosio_assert(deal_to == deal_idx.end(),"the deal has exist");

   deal_tbl.emplace( _self, [&]( auto& s ) {
      s.id = deal_tbl.available_primary_key();
      s.current_block = current_block_num();
      s.filemd5 = to->filemd5;
      s.base = to->base;
      s.quote.assign(to->quote.begin(),to->quote.end());
      s.record_property = to->record_property;
   });

   idx.erase(to);

}
void token::cancel(string record_id,string memo) {
   eosio_assert(record_id != string(""),"the record id must not be empty");
   require_auth( _self);
   exchange_records record_tbl(_self,_self);

   auto idx = record_tbl.get_index<N(byhash)>();
   auto to = idx.find(get_key_id(record_id));
   eosio_assert(to != idx.end(),"the contract do not exist");

   INLINE_ACTION_SENDER(jiaozi::token, transfer)( _self, {{_self,N(active)}},
                                 { _self,to->base.trader,to->base.quantity,memo} );
   for (auto itr = to->quote.begin();itr != to->quote.end(); ++itr) {
      INLINE_ACTION_SENDER(jiaozi::token, transfer)( _self, {{_self,N(active)}},
                           { _self,itr->trader,itr->quantity,memo} );
   }

   idx.erase(to);
}

} /// namespace eosio

EOSIO_ABI( jiaozi::token, (create)(issue)(transfer)(fee)(setprice)(destroy)(setvalue)(calvalue)(calprice)(exchange)(confirm)(cancel) )
